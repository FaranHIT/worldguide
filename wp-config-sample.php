<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'worldguide' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8JS^_|%o<Oz<v`4R>{_-Aojw+N2]j~GV4bOHV)|z&A0Zt5>l|!^A,<7v%Mog2Eyl' );
define( 'SECURE_AUTH_KEY',  ']9^4[*nbpHy-MF`L2kY1U}+VXjo-s=+O1!#-8[as0u4>;:[s8h.6!B022z0Qzzmq' );
define( 'LOGGED_IN_KEY',    '?!tqI})-lP$_1h9}B9b{=`un7.qX.ZV.+[cM3Ez^H2i.X`B2=S4CjhQaX0u A0kn' );
define( 'NONCE_KEY',        'Q{)Fw,UVk0wC1Ut*?Bk[f2X{y#EH3l?@Mt<C*Fi:~GV^ssBrI7q;/hi}r@Q4&!9j' );
define( 'AUTH_SALT',        'JT^BKTn QWGe=vanYIPKgkhmbj:sM3*9Q|e{c|OIDQjJBIG={9JqkX,c%Zs7TmJa' );
define( 'SECURE_AUTH_SALT', '9/|.F7@[0F&s={c=]IychM12/ri=K_or9;)XG]K?r87QgzD=OS~1)oM1#xFb/Srn' );
define( 'LOGGED_IN_SALT',   'RO0e SPPMFZD6`kv,DCo`eU:aUE7ys@/Dt:6 )kt+Wn&3` -n>,TjtddqnKDm<J:' );
define( 'NONCE_SALT',       'rJAe6kJ|#>%%g,<<<Pl>##9@4wASb0N,1Hd=c55YS>%K,W8HL<K|h_uE,1-(;g]#' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
