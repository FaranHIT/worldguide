<?php 

use \Firebase\JWT\JWT;


    add_action('rest_api_init', function () {

        register_rest_route( 'listingpro/v1', 'get_all_users',array(

            'methods'  => 'get',
            'callback' => 'get_all_users'
        ));
        
        register_rest_route( 'listingpro/v1', 'get_all_users_filterd',array(

            'methods'  => 'post',
            'callback' => 'get_all_users_filterd'
        ));
        
        register_rest_route( 'listingpro/v1', 'send_friend_request',array(

            'methods'  => 'post',
            'callback' => 'send_friend_request'
        ));
        
        register_rest_route( 'listingpro/v1', 'get_friend_requests',array(

            'methods'  => 'get',
            'callback' => 'get_friend_requests'
        ));
        
        register_rest_route( 'listingpro/v1', 'accept_reject_request',array(

            'methods'  => 'post',
            'callback' => 'accept_reject_request'
        ));

    });
    
    
    function get_all_users($request){
        
        global $wpdb;
        
        $all_users = get_users(array( 'fields' => array( 'ID','display_name' ) ));
      
        
        foreach ( $all_users as $user ) {
            $sender_id = get_current_user_id();
            $checkSentRequest = $wpdb->get_row( "SELECT * FROM wp_friends WHERE user1 = $sender_id  and user2 = $user->ID");  
            
            $user->request_status = 0;
            if($checkSentRequest){
                $user->request_status = 1;
            }
            
            $user->nationality = get_the_author_meta('nationality', $user->ID);
            
            $author_avatar_url = get_user_meta($user->ID, "listingpro_author_img_url", true); 

				if(!empty($author_avatar_url)) {

					$user->profile_picture =  $author_avatar_url;

				} else { 			

					$avatar_url = listingpro_get_avatar_url ( $user->ID, $size = '94' );
					 $user->profile_picture =  $avatar_url;

				}
		   
				
				
				
	//======================distance calculation========================//
	
	
	    $otherUserLatitude =  get_the_author_meta('latitude', $user->ID);  
        $otherUserLongitude =  get_the_author_meta('longitude', $user->ID);

        $currentUserLatitude  = get_the_author_meta('latitude', get_current_user_id()); 
        $currentUserLongitude  = get_the_author_meta('longitude', get_current_user_id());

        $latitudeFrom = $currentUserLatitude;
        $longitudeFrom = $currentUserLongitude;
        $latitudeTo = $otherUserLatitude;
        $longitudeTo = $otherUserLongitude;
        $earthRadius = 6371000;

        $theta = $longitudeFrom - $longitudeTo; 
        $distance = (sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo))) + (cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta))); 
        $distance = acos($distance); 
        $distance = rad2deg($distance); 
        $distance = $distance * 60 * 1.1515;

     // $distance = $distance * 1.609344;
        if($latitudeTo && $longitudeTo != ""){

          $user->miles_away = round($distance,2); 
        }
        else
        {
            $user->miles_away = 0; 

        }

     //======================distance calculation========================//
				
           $user->country =  get_the_author_meta('country', $user->ID);
           $user->city =  get_the_author_meta('city', $user->ID);
           $user->languge =  get_the_author_meta('language', $user->ID);
        }
       
        
        $response = new WP_REST_Response($all_users);
        $response->set_status( 200 );
        return $response;

    }
    
    /*function get_all_users_filterd($request){
        global $wpdb;
        $FilterdUsers = [];
        
        // =============== | Params | ===============
            $country = (empty($request->get_param('country'))?'':$request->get_param('country'));
            $nationality = (empty($request->get_param('nationality'))?'':$request->get_param('nationality'));
            $language = (empty($request->get_param('language'))?'':$request->get_param('language'));
            $city = (empty($request->get_param('city'))?'':$request->get_param('city'));
        // ==========================================
        
        $query = "SELECT u.Id, u.display_name, um.meta_key, um.meta_value FROM wp_users u INNER JOIN wp_usermeta um ON u.ID = um.user_id WHERE (".(!empty($country) ? '(um.meta_key = \'country\' AND um.meta_value = \''.$country.'\')' : '(um.meta_key = \'country\')').(!empty($nationality) ? ' OR (um.meta_key = \'nationality\' AND um.meta_value = \''.$nationality.'\')' : '').(!empty($language) ? ' OR (um.meta_key = \'language\' AND um.meta_value = \''.$language.'\')' : '').(!empty($city) ? ' OR (um.meta_key = \'city\' AND um.meta_value = \''.$city.'\')' : '').") GROUP BY u.Id, u.display_name, um.meta_key, um.meta_value";
        //echo$query;
        //exit;
        $Selected_Users = $wpdb->get_results($query);
    
        $request_status = 0;
        if($Selected_Users){
            $request_status = 1;
        }

        foreach($Selected_Users as $filterdUsers){
            $FilterdUsers[$filterdUsers->display_name] = array();
        }
        foreach($Selected_Users as $Results){
            (!array_key_exists('ID', $Results)) ? $FilterdUsers[$Results->display_name] += [ 'ID' => $Results->Id] : '';
            
            $FilterdUsers[$Results->display_name] += [
                        'display_name' => $Results->display_name
                    ];
            
            $FilterdUsers[$Results->display_name] += [
                        'request_status' => $request_status
                    ];
                    
            $FilterdUsers[$Results->display_name] += [
                        'profile_picture' => get_the_author_meta('profile_picture', $Results->Id)
                    ];
            
            $FilterdUsers[$Results->display_name] += [
                        $Results->meta_key => $Results->meta_value
                    ];
            
            $FilterdUsers[$Results->display_name] += [
                        'miles_away' => Distance_Calculation($Results->Id)
                    ];
        }
        
        //var_dump($FilterdUsers);
        
        $counter = 0;
        $Results = [];
        $args = [];
        
        $args = array_filter([$country, $nationality, $language, $city]);
    
        return multi_array_search($FilterdUsers, $_GET);

    }
    
    function multi_array_search($array, $search){
    $result = array();
    foreach ($array as $key => $value)
    {
      foreach ($search as $k => $v)
      {
        if ((!isset($value[$k]) && $value[$k] !== null) || $value[$k] != $v)
        {
          continue 2;
        }
      }
      $result[] = $array[$key];
    }
    $response = new WP_REST_Response($result);
    $response->set_status( 200 );
    return $response;
  }*/
  
  function Distance_Calculation($userId){
        $otherUserLatitude =  get_the_author_meta('latitude', $userId);  
        $otherUserLongitude =  get_the_author_meta('longitude', $userId);
    
        $currentUserLatitude  = get_the_author_meta('latitude', get_current_user_id()); 
        $currentUserLongitude  = get_the_author_meta('longitude', get_current_user_id());
    
        $latitudeFrom = $currentUserLatitude;
        $longitudeFrom = $currentUserLongitude;
        $latitudeTo = $otherUserLatitude;
        $longitudeTo = $otherUserLongitude;
        $earthRadius = 6371000;
    
        $theta = $longitudeFrom - $longitudeTo; 
        $distance = (sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo))) + (cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta))); 
        $distance = acos($distance); 
        $distance = rad2deg($distance); 
        $distance = $distance * 60 * 1.1515;
    
     // $distance = $distance * 1.609344;
        if($latitudeTo && $longitudeTo != ""){
    
            $miles_away = round($distance,2); 
        }
        else
        {
            $miles_away = 0; 
    
        }
        return $miles_away;
  }
    
    
    
    
    
    
    
    
    function send_friend_request($request){
        global $wpdb;
        $sender_id = get_current_user_id();
        $reciever_id = $request->get_param('user_id');
        
        $checkSentRequest = $wpdb->get_row( "SELECT * FROM wp_friends WHERE user1 = $sender_id  and user2 = $reciever_id");
        
        if($checkSentRequest){
            $response = new WP_REST_Response(["message" =>'You have already sent Friend request to this user!']);
            $response->set_status( 403 );
            return $response;
        }
        
        $wpdb->insert( 
            'wp_friends', 
            array( 
                'user1' => $sender_id, 
                'user2' => $reciever_id 
            ), 
            array( 
                '%d', 
                '%d' 
            ) 
        );
        
        $response = new WP_REST_Response(["message" =>'Friend Request Sent successfully']);
        $response->set_status( 400 );
        return $response;
    }
    
    function get_friend_requests($request){
        global $wpdb;
        $user_id = get_current_user_id();
        
  
        
         $all_requests = $wpdb->get_results( "SELECT `wp_friends`.`id`, `wp_friends`.`user1`, `wp_friends`.`user2` ,`user`.`display_name` FROM `wp_friends` INNER JOIN `wp_users` AS `user` ON `wp_friends`.`user1` = `user`.`ID` WHERE `wp_friends`.`user2` = $user_id AND `wp_friends`.`status` = 0" );
         
         foreach($all_requests as $request){
             
             $author_avatar_url = get_user_meta($request->user1, "listingpro_author_img_url", true); 

				if(!empty($author_avatar_url)) {

					$request->profile_picture =  $author_avatar_url;

				} else { 			

					$avatar_url = listingpro_get_avatar_url ( $user_id, $size = '94' );
					 $request->profile_picture =  $avatar_url;

				}
         }
        
    
        $response = new WP_REST_Response($all_requests);
        $response->set_status( 200 );
        return $response;
    }
    
    function accept_reject_request($request){
        global $wpdb;
        $request_id = $request->get_param('request_id');
        $action = $request->get_param('action');
        $wpdb->update( 
        	'wp_friends', 
        	array( 
        		'status' => $action,	
        	), 
        	array( 'id' => $request_id ), 
        	array( 
        		'%d',
        	), 
        	array( '%d' ) 
        );
        
        if($action == 1){
            $response = new WP_REST_Response(["message" =>'Accepted!']);
        }
        if($action == 2){
            $response = new WP_REST_Response(["message" =>'Rejected!']);
        }
        
        
        $response->set_status( 200 );
        return $response;
        
    }

?>