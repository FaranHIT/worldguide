<?php 
add_action('rest_api_init', function () {
    
          register_rest_route( 'listingpro/v1', 'about_us',array(
                        'methods'  => 'GET',
                        'callback' => 'about_us'
          ));
          
          register_rest_route( 'listingpro/v1', 'page',array(
                        'methods'  => 'GET',
                        'callback' => 'page'
          ));
          
        //   register_rest_route( 'listingpro/v1', 'city',array(
        //                 'methods'  => 'GET',
        //                 'callback' => 'city'
        //   ));

          
    });

    function about_us($request) {
    
        $args = array(
                'category' => $request['post_id']
        );
        
    
        $posts = get_post(39);
        if (empty($posts)) {
        return new WP_Error( 'empty_category', 'there is no post in this category', array('status' => 404) );
    
        }
        
       
       
        $response = new WP_REST_Response($posts);
        $response->set_status(200);
    
        return $response;
    }
    
    function page($request) {
    
       $page_title = $request->get_param('slug');
       
        WPBMap::addAllMappedShortcodes();
        
        global $post;
        $post = get_page_by_path($page_title);
        
        
        $output['post_title'] = $post->post_title;
        $output['post_content'] = apply_filters( 'the_content', $post->post_content );
         
       
    
        $response = new WP_REST_Response($output);
        $response->set_status(200);
    
        return $response;
    }
    
    // function city($request) {
    
    //   $page_title = $request->get_param('slug');
       
    //     WPBMap::addAllMappedShortcodes();
        
    //     global $post;
    //     $post = get_page_by_path($page_title);
        
        
    //     $output['post_title'] = $post->post_title;
    //     $output['post_content'] = apply_filters( 'the_content', $post->post_content );
         
       
    
    //     $response = new WP_REST_Response($output);
    //     $response->set_status(200);
    
    //     return $response;
    // }


?>