<?php 

	add_action('rest_api_init', function () {

        register_rest_route( 'listingpro/v1', 'blogs',array(

            'methods'  => 'get',
            'callback' => 'get_blogs'
        ));

        register_rest_route( 'listingpro/v1', 'blog/(?P<blog_id>\d+)',array(

            'methods'  => 'get',
            'callback' => 'get_blog'
        ));


    });

    /* ============== Blog List ============ */


    function get_blogs(){

        $args = array(
            'posts_per_page'   => 10,
            'category'         => 256,
            
        );

    	$blogs = get_posts($args);

    	foreach ($blogs as $blog) {
    		$blog->Blog_image = get_the_post_thumbnail_url($blog->ID,array(372,240));
            $author_avatar_url = get_user_meta($blog->post_author, "listingpro_author_img_url", true); 
            // $blog->author_image =  "";
            if(!empty($author_avatar_url)) {

                $blog->author_image =  $author_avatar_url;

            }
            else{
                $blog->author_image = listingpro_get_avatar_url ( $blog->post_author, $size = '94' );
                    
            }
    		$blog->post_author = get_author_name( $blog->post_author);
    	}
    	//$blogs1 = add_filter( 'rest_prepare_post', 'blogs', 10, 3 );

    	 $response = new WP_REST_Response( $blogs );
         $response->set_status( 200 );
         return $response;

    }

    /* ============== Blog Detail ============ */

    function get_blog($request){

    	$blog = get_post($request->get_param('blog_id'));

		$blog->Blog_image = get_the_post_thumbnail_url($blog->ID,array(372,240));
		$blog->author_image = get_avatar_url( $blog->post_author);
		$blog->post_author = get_author_name( $blog->post_author);

    	
    	 $response = new WP_REST_Response( $blog );
         $response->set_status( 200 );
         return $response;

    }



?>