<?php 

use \Firebase\JWT\JWT;


    add_action('rest_api_init', function () {

        register_rest_route( 'listingpro/v1', 'login',array(

            'methods'  => 'post',
            'callback' => 'login'
        ));

        register_rest_route( 'listingpro/v1', 'register',array(

            'methods'  => 'post',
            'callback' => 'register'
        ));

        register_rest_route( 'listingpro/v1', 'forgot_password',array(

            'methods'  => 'post',
            'callback' => 'forgot_password'
        ));


    });

    /* ============== Login ============ */

    function login($request)
    {
        $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
        $username = $request->get_param('username');
        $password = $request->get_param('password');

        /** First thing, check the secret key if not exist return a error*/
        if (!$secret_key) {
            return new WP_Error(
                'jwt_auth_bad_config',
                __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
                array(
                    'status' => 403,
                )
            );
        }
        /** Try to authenticate the user with the passed credentials*/
        $user = wp_authenticate($username, $password);

        /** If the authentication fails return a error*/
        if (is_wp_error($user)) {
            $error_code = $user->get_error_code();
            return new WP_Error(
                '[jwt_auth] ' . $error_code,
                $user->get_error_message($error_code),
                array(
                    'status' => 403,
                )
            );
        }

        /** Valid credentials, the user exists create the according Token */
        $issuedAt = time();
        $notBefore = apply_filters('jwt_auth_not_before', $issuedAt, $issuedAt);
        $expire = apply_filters('jwt_auth_expire', $issuedAt + (DAY_IN_SECONDS * 7), $issuedAt);

        $token = array(
            'iss' => get_bloginfo('url'),
            'iat' => $issuedAt,
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => array(
                'user' => array(
                    'id' => $user->data->ID,
                ),
            ),
        );

        /** Let the user modify the token data before the sign. */
        $token = JWT::encode(apply_filters('jwt_auth_token_before_sign', $token, $user), $secret_key);

        /** The token is signed, now create the object with no sensible user data to the client*/
        $data = array(
            'token' => $token,
            'user_id' => $user->id,
            'user_email' => $user->data->user_email,
            'user_nicename' => $user->data->user_nicename,
            'user_display_name' => $user->data->display_name,
        );

        /** Let the user modify the data before send it back */
        return apply_filters('jwt_auth_token_before_dispatch', $data, $user);
    }

    /* ============== Register ============ */

    function register($request){

        $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
        $user_login = $request->get_param('username');
        $user_email = $request->get_param('email');
        $first_name = $request->get_param('first_name');
        $last_name = $request->get_param('last_name');
        $phone = $request->get_param('phone');

        $user_password = $request->get_param('password');

        $imgdata   =   base64_decode($request->get_param('image'));

        // $password = '123456';
        $userdata = array(
            'user_email'            => $user_email,
            'user_pass'             => $user_password,   
            'user_login'            => $user_login,   
            'first_name'            => $first_name,   
            'last_name'             => $last_name,   
            'phone'                 => $phone,   
            'role'                  => 'subscriber'
        );

        $user = wp_insert_user( $userdata );


//var_dump($user);



        if (is_wp_error($user)) {
            $error_code = $user->get_error_code();
            return new WP_Error(
                '[jwt_auth] ' . $error_code,
                $user->get_error_message($error_code),
                array(
                    'status' => 403,
                )
            );
        }

        $issuedAt = time();
        $notBefore = apply_filters('jwt_auth_not_before', $issuedAt, $issuedAt);
        $expire = apply_filters('jwt_auth_expire', $issuedAt + (DAY_IN_SECONDS * 7), $issuedAt);

    $f = finfo_open();
    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
    $type_file = explode('/', $mime_type);
    $avatar = time() . '.' . $type_file[1];

    $uploaddir = wp_upload_dir(); 
    $myDirPath = $uploaddir["path"]; 
    $myDirUrl = $uploaddir["url"];

    file_put_contents($uploaddir["path"].'/'.$avatar,$imgdata);

    $filename = $myDirUrl.'/'.basename( $avatar );
    $wp_filetype = wp_check_filetype(basename($filename), null );
    $uploadfile = $uploaddir["path"] .'/'. basename( $filename );           

    $attachment = array(
     "post_mime_type" => $wp_filetype["type"],
     "post_title" => preg_replace("/\.[^.]+$/", "" , basename( $filename )),
     "post_content" => "",
     "post_status" => "inherit",
     'guid' => $uploadfile,
     );              

    require_once(ABSPATH . '/wp-load.php');             
    require_once(ABSPATH . 'wp-admin' . '/includes/file.php');
    require_once(ABSPATH . 'wp-admin' . '/includes/image.php');
    $attachment_id = wp_insert_attachment( $attachment, $uploadfile );
    $attach_data = wp_generate_attachment_metadata( $attachment_id, $uploadfile );
    wp_update_attachment_metadata( $attachment_id, $attach_data );

    update_post_meta($attachment_id,'_wp_attachment_wp_user_avatar',$user);
    update_user_meta($user, 'wp_user_avatar', $attachment_id);

         $token = array(
            'iss' => get_bloginfo('url'),
            'iat' => $issuedAt,
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => array(
                'user' => array(
                    'id' => $user,
                ),
            ),
        );

        /** Let the user modify the token data before the sign. */
        $token = JWT::encode(apply_filters('jwt_auth_token_before_sign', $token, $user), $secret_key);


         $data = array(
            'token' => $token,
            'user_id' => $user,
            
        );

         // return apply_filters('jwt_auth_token_before_dispatch', $data, $user);
         $response = new WP_REST_Response( $data );
         $response->set_status( 200 );
         return $response;


    }

    /* ============== Forgot Password ============ */

    function forgot_password($request){

        $user_email = $request->get_param('email');

        if ( email_exists($user_email) != false && !empty($user_email) && is_email($user_email) ) {
                    $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
                    
                    $userData = get_user_by( 'email', $user_email );
                    $userID = $userData->ID;
                    wp_set_password( $random_password, $userID );
                    
                    $subject = esc_html__('Password changed', 'listingpro');
                    $content = esc_html__('You new password is: ', 'listingpro');
                    $content .= $random_password.'<br>';
                    $content .= esc_html__('Kindly update your password from your dashboard profile', 'listingpro');
                    lp_mail_headers_append();
                    $headers[] = 'Content-Type: text/html; charset=UTF-8';
                    wp_mail( $user_email ,  $subject , $content , $headers );
                    lp_mail_headers_remove();
                    // $note = '<span class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> '.esc_html__('Go to your inbox or spam/junk and get your password','listingpro').'</span>';

                     $response = new WP_REST_Response( ['message'=>'Password Sent!, Go to your inbox or spam/junk and get your password'] );
                     $response->set_status( 200 );
                     return $response;
                    
                }
                else{
                    $response1 = new WP_REST_Response( ['message'=>'Email not found in users list']);
                     $response1->set_status( 403 );
                     return $response1;

                }

    }

?>